clear;


% parameters
epsilon = 0.1;
L = 2;
t_final = 1;
t = linspace(0, t_final, 1000);


% Parameters
epsilon = 0.1;
L = 2;


% Exact solution of Burgers' equation
solution = @(x, t) -tanh((x + 0.5 - t) / (2 * epsilon)) + 1;


%%
% Grid width parameters for convergence test
grid_widths = 1 ./ (2.^(2:10)); % Grid widths: 0.25, 0.125, 0.0625, ...

% Arrays to store convergence results
errors = zeros(size(grid_widths));

% Iterate over different grid widths
for i = 1:length(grid_widths)
    disp("solving for grid width: ");
    disp(grid_widths(i));
    dx = grid_widths(i);

    % Spatial grid
    x = linspace(-1, -1 + L, round(L / dx) + 1);

    % Initial condition
    IC = solution(x, 0);

    % Solve using ode15s with burgers_ode function
    [~, u] = ode15s(@fpde, t, IC, [], dx, epsilon);

    % reconstructing the boundaries
    u_left = -tanh((-1 + 0.5 - t) / (2 * epsilon)) + 1;
    u_right = -tanh((1 + 0.5 - t) / (2 * epsilon)) + 1;

    u(:, 1) = u_left;
    u(:, end) = u_right;

    % Calculate error (L2 norm) between numerical solution and exact solution
    u_exact = solution(x, 1); % Evaluate exact solution at t = 1
    error = norm(u(end, :) - u_exact, 2) / sqrt(length(u_exact)); % L2 norm error
    errors(i) = error;
end
loglog(grid_widths, errors)
%%

% discretisation
function dudt = fpde(t, u, dx, epsilon)
    Nx = length(u);

    lambda1 = epsilon ./ (dx^2);
    lambda2 = 1 ./ (2*dx);
    
    dudt = zeros(Nx, 1);
    u(1) = -tanh((-1 + 0.5 - t) / (2 * epsilon)) + 1;
    u(end) = -tanh((1 + 0.5 - t) / (2 * epsilon)) + 1;
    for i = 2:Nx-1
        dudt(i) = lambda1 .* (u(i+1) - 2.*u(i) + u(i-1)) - lambda2 .* u(i) ...
            .*  (u(i + 1) - u(i - 1));
    end
    u(1) = -tanh((-1 + 0.5 - t) / (2 * epsilon)) + 1;
    u(end) = -tanh((1 + 0.5 - t) / (2 * epsilon)) + 1;
end








