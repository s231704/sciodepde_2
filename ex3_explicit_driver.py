import numpy as np
import matplotlib.pyplot as plt
from scipy.sparse import diags

a = 0.5
def solution(x, t):
    return np.sin(2 * np.pi * (x - a * t))

# Function to construct the sparse matrix A for the FTCS scheme
def construct_A(alpha, dx, dt, nx):
    s = alpha * dt / dx

    # Create diagonals for the FTBS scheme
    main_diag = (1 - s) * np.ones(nx)
    sub_diag = s * np.ones(nx - 1)

    diagonals = [main_diag, sub_diag]
    offsets = [0, -1]
    A = diags(diagonals, offsets, shape=(nx, nx)).toarray()
    return A

'''
    # Plot eigenvalues on the complex plane
    plt.plot(eigenvalues.real, eigenvalues.imag, 'o', markersize=8)
    plt.axhline(0, color='black', linewidth=0.5)
    plt.axvline(0, color='black', linewidth=0.5)
    plt.xlabel('Real Part')
    plt.ylabel('Imaginary Part')
    plt.title('Eigenvalues on Complex Plane')
    plt.grid(True)
    plt.show()
    # Boundary conditions'''

# Function to solve the 1D heat equation using the FTCS scheme with sparse matrix
def solve_advection_problem_FTBS(u0, a, dx, dt, t_final, boundary_conditions, solution):
    nx = len(u0)
    A = construct_A(a, dx, dt, nx)
    X = np.linspace(-1, 1, nx + 2)
    timesteps = int(t_final / dt)
    time_error = np.zeros(timesteps + 1)
    for curr_time in range(timesteps):
        u0 = A @ u0
        u0[0] += boundary_conditions[0][curr_time]*(a * dt / dx)
        iter_space_err = np.abs(np.concatenate(([0], np.array(u0) - [solution(x, dt * (curr_time)) for x in X][1:-1], [0]))) 
        time_error[curr_time + 1] = np.sum(np.sqrt(iter_space_err ** 2)/ len(iter_space_err))
        #time_error[curr_time + 1] = np.min(iter_space_err)
    solved_u = np.concatenate(([boundary_conditions[0][-1]], u0, [boundary_conditions[1][-1]]))
    spacial_error = np.abs(solved_u - [solution(x, t_final) for x in X])
 
    return solved_u, spacial_error, time_error