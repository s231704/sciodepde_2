clear;


% parameters
epsilon = 0.01/pi;
%t_final = 1.6037/pi;
t_final = 1.6037/pi;
t = linspace(0, t_final, 1000);


% Parameters
L = 2;


% Exact solution of Burgers' equation
solution = @(x, t) -sin(pi * x);


%%
% Grid width parameters for convergence test
dx = (2.^-6); % Grid widths: 0.25, 0.125, 0.0625, ...

% Arrays to store convergence results



% Spatial grid
x = linspace(-1, -1 + L, round(L / dx) + 1);

% Initial condition
IC = solution(x, 0);

% Solve using ode15s with burgers_ode function
[~, u] = ode15s(@fpde, t, IC, [], dx, epsilon);

    % reconstructing the boundaries
u_left = t .* 0;
u_right = t .* 0;

u(:, 1) = u_left;
u(:, end) = u_right;
dudx = deriveU(u(end, :), dx, round(L / dx) + 1);



function dudx = deriveU(u, dx, N)
dudx = zeros(N);

for i=1:N-1
    dudx(i) =  (u(i+1) - u(i))/dx;
end
dudx(N) = dudx(N-1);

end

%%

% discretisation
function dudt = fpde(t, u, dx, epsilon)
    Nx = length(u);

    lambda1 = epsilon ./ (dx^2);
    lambda2 = 1 ./ (2*dx);
    
    dudt = zeros(Nx, 1);
    u(1) = 0;
    u(end) = 0;
    for i = 2:Nx-1
        dudt(i) = lambda1 .* (u(i+1) - 2.*u(i) + u(i-1)) - lambda2 .* u(i) ...
            .*  (u(i + 1) - u(i - 1));
    end
end








