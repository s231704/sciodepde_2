import numpy as np
import matplotlib.pyplot as plt
from scipy.sparse import diags
from scipy.sparse.linalg import spsolve
import sys

from ex2_explicit_driver import solve_heat_equation_FTCS



def printHelp():
    print("run options \n1: general testing of the solver (section 1)")
    print("2: convergence test over spacial grid (seciton 2)")
    print("3: convergence test over temporal grid (section 3)")

if len(sys.argv) < 2:
    printHelp()
    quit()

runMode = sys.argv[1]





# solution
epsilon = 0.1
alphas = [1, 4, 16]
ex_component = lambda t, alpha : np.exp(-epsilon * alpha**2 * t)
cos_component = lambda x, alpha : np.cos(alpha * x)
def solution(x, t):
    ex_array = [ex_component(t, alpha) for alpha in alphas]
    cos_array = [cos_component(x, alpha) for alpha in alphas]
    return np.dot(ex_array, cos_array)

# functions parameters
L = 2.0         # Length of the rod
t_final = 1    # Final time
epsilon = 0.1        # Thermal diffusivity

if runMode == "1":

    ##################################################################################################
    ########     seciotn 1) Standard solver, for  atomic isolated tests                    ###########
    ##################################################################################################

    # fixed temporal grid
    nt = 16       # Number of time steps
    dt = t_final / (nt)
    T = np.linspace(0, t_final, nt + 1)

    # Boundary conditions
    bc_left = [solution(-1, t) for t in T]
    bc_right = [solution(1, t) for t in T]
    boundary_conditions = (bc_left, bc_right)



    nx = 9
    dx = 1 / (nx - 1)
    print("dt = ", dt, " dx = ", dx)
    print("s = ", (epsilon * dt) / dx**2)
    X =  np.linspace(-1, -1 + L, int(L / dx + 1))
    u0 = [solution(x, 0) for x in X][1:-1]
    solve_u, iter_err, _ = solve_heat_equation_FTCS(u0, epsilon, dx, dt, t_final, boundary_conditions, solution)
    fig, (ax1, ax2) = plt.subplots(1, 2)
    ax1.plot(X, solve_u)
    ax1.plot(X, [solution(x, t_final) for x in X], "--")
    ax2.plot(X, iter_err)
    ax2.set_ylabel("|U - u_exact|")
    ax2.set_xlabel("x")
    ax1.legend(["U", "u_exact"])
    ax1.set_xlabel("x")
    ax1.set_title("unstable FTCS run")
    ax2.set_title("error profile")
    plt.show()

    ###########################################################################################

elif runMode == "2":
    #########################################################################################
    ######## Convergence test, for spacial step size (different spacial step size)###########
    #########################################################################################

    # fixed temporal grid
    nt = 250000       # Number of time steps
    dt = t_final / (nt)
    print("dt = ", dt)
    T = np.linspace(0, t_final, nt + 1)

    # Boundary conditions
    bc_left = [solution(-1, t) for t in T]
    bc_right = [solution(1, t) for t in T]
    boundary_conditions = (bc_left, bc_right)

    # running the simulation
    err_space = np.zeros(7)
    spacial_grid_whidth = [1 / 2**h for h in range(3, 10)]

    print("--------------- Running convergence test for spacial grid --------------------------")

    for i, h in enumerate(spacial_grid_whidth):
        print("solving for h = ", h)
        u0 = [solution(x, 0) for x in np.linspace(-1, -1 + L, int(L / h) + 1)][1:-1]
        solve_u, iter_err, _ = solve_heat_equation_FTCS(u0, epsilon, h, dt, t_final, boundary_conditions, solution)
        err_space[i] = np.sqrt(np.sum((np.power(iter_err, 2)) / len(iter_err))) 


    plt.loglog(spacial_grid_whidth, err_space, "-o")
    plt.loglog(spacial_grid_whidth, np.power(spacial_grid_whidth, 2), "--")
    plt.xlabel("h")
    plt.ylabel("||E||_2")
    plt.legend(["error" ,"O(h^2)"])
    plt.title("FTCS convergence test for spacial grid h")
    plt.show()
    ###########################################################################################

elif runMode == "3":
    ###########################################################################################
    ######## Convergence test, for temporal step size (different temporal step size)###########
    ###########################################################################################

    # fixed spacial grid
    nx = 32 + 1
    dx = L / (nx - 1)
    print(dx)
    X = np.linspace(-1, -1 + L, nx)
    u0 = [solution(x, 0) for x in X][1:-1]

    # running the simulation
    err_time = np.zeros(5)
    grid_whidth = [1 / 2**h for h in range(6, 11)]

    print("--------------- Running convergence test for temporal grid --------------------------")

    for i, k in enumerate(grid_whidth):
        print("solving for k = ", k)
        nt = int(t_final / k)

        # Boundary conditions
        T = np.linspace(0, t_final, nt + 1)
        bc_left = [solution(-1, t) for t in T]
        bc_right = [solution(1, t) for t in T]
        boundary_conditions = (bc_left, bc_right)
        print(dx, "----", k)

        solve_u, special, iter_err = solve_heat_equation_FTCS(u0, epsilon, dx, k, t_final, boundary_conditions, solution)
        #err_time[i] = np.sum(np.sqrt(iter_err ** 2) / len(iter_err))
        err_time[i] = np.max(iter_err)
    print(err_time)
    plt.loglog(grid_whidth, err_time, "-o")
    plt.loglog(grid_whidth, np.power(grid_whidth, 1) * 20, "--")
    plt.xlabel("k")
    plt.ylabel("||E_n||_inf")
    plt.legend(["error" ,"O(k)"])
    plt.title("FTCS convergence test for temporal grid k")
    plt.show()

    ###########################################################################################