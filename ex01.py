import numpy as np
import math
import matplotlib.pyplot as plt
import scipy as sp

# Our RK23 solver
def rk23(f,tspan,y0,reps,aeps):
    c1 = 0.
    c2 = 0.5
    c3 = 1.
    a21 = 0.5
    a31 = 0.5
    a32 = 0.5
    b1 = 1/6
    b2 = 2/3
    b3 = 1/6
    d1 = b1-0
    d2 = b2-0.5
    d3 = b3-0.5
    t0 = tspan[0]
    tend = tspan[1]
    t = t0
    y = y0
    Y = [y0]
    T = [t0]
    h = 1/np.abs(np.max(f(t,y)))
    tol = aeps + reps*np.abs(np.max(y))
    while abs(t-tend) > 1e-12:
        k_1 = y
        hf1 = h*f(t,k_1)
        k2 = y + a21*hf1
        hf2 = h*f(t+c2*h,k2)
        k3 = y + a31*hf1 + a32*hf2
        hf3 = h*f(t+c3*h,k3)
        ynew = y + b1*hf1 + b2*hf2 + b3*hf3
        tol = aeps + reps*np.abs(np.max(ynew))
        E = np.abs(np.max(d1*hf1 + d2*hf2 + d3*hf3))
        if E < tol:
            t += h
            y = ynew
            T.append(t)
            Y.append(y)
        else:
            h = 0.9*h*(tol/E)**(1/3)
        h = min([h, tend - t])
    return T,Y

# The flame propagation model
def flamepropagation(t,y):
    y = y[0]
    return np.asarray(y**2 - y**3)

Delta = [0.02, 0.002, 0.0001]

fig, axs = plt.subplots(1,len(Delta),figsize=(10, 4))
axs[0].set_ylabel('y')

for i,delta in enumerate(Delta):
    tspan = [0, 2./delta]
    y0 = [delta]
    T,Y = rk23(flamepropagation, tspan, y0, 1e-4,1e-4)
    axs[i].plot(T,Y,'o',markersize=1)
    axs[i].set_xlabel('t')
    axs[i].title.set_text(f'$\\delta = {delta}$')

plt.savefig('ex01_different_delta.png', bbox_inches='tight',dpi=300)

# Solution by library solver
sol = sp.integrate.solve_ivp(flamepropagation, tspan, y0, method='RK23',rtol=1e-4,atol=1e-4)
print(f"number of steps of implemented function: {len(T)}\nnumber of steps of library function:{len(sol.t)}")

fig, axs = plt.subplots(1,2,figsize=(10, 4))
axs[0].plot(T,Y,'o',markersize=1)
axs[0].set_xlabel('t')
axs[0].set_ylabel('y')
axs[0].title.set_text(f'implemented')
axs[1].plot([sol.t],sol.y,'o',markersize=1,color='#1f77b4')
axs[1].set_xlabel('t')
axs[1].title.set_text(f'library function')
plt.savefig('ex01_compare.png', bbox_inches='tight',dpi=300)
plt.show()