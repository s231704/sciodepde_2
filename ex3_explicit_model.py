import numpy as np
import matplotlib.pyplot as plt
from scipy.sparse import diags
import sys

from ex3_explicit_driver import solve_advection_problem_FTBS


def printHelp():
    print("run options , add one of the following command line arguments: \n0: predictive test and verification Ex 3.3")
    print("1: Diffusion test")
    print("2: convergence test over spacial grid (seciton 2)")
    print("3: convergence test over temporal grid (section 3)")

if len(sys.argv) < 2:
    printHelp()
    quit()

runMode = sys.argv[1]
a = 0.5
def solution(x, t):
    return np.sin(2 * np.pi * (x - a * t))


# Parameters
L = 2.0         # Length of the rod
t_final = 1  # Final time

if runMode == "0":
    ##################################################################################################
    ########     seciotn 0) Standard solver, for  atomic isolated tests                    ###########
    ##################################################################################################

    # fixed temporal grid
    #section for exercice 3.3
    t_final = 40
    nx = 100 * 2 - 1
    dx = L / (nx - 1)
    dt = 0.8*dx / a
    nt = int(t_final / dt + 1)

    #nt = 4000       # Number of time steps
    
    T = np.linspace(0, t_final, nt)
    # Boundary conditions
    bc_left = [solution(-1, t) for t in T]
    bc_right = [solution(1, t) for t in T]
    
    boundary_conditions = (bc_left, bc_right)

    # running the simulation
    err_space = np.zeros(6)
    

    print("h = ", dx, " k = ", dt)
    print("Cr = ", 0.5 * dt / dx)
    X =  np.linspace(-1, -1 + L, nx)
    u0 = [solution(x, 0) for x in X][1:-1]
    solve_u, iter_err, _ = solve_advection_problem_FTBS(u0, a, dx, dt, t_final, boundary_conditions, solution)
    u_exact = [solution(x, t_final) for x in X]
  #  fig, (ax1, ax2) = plt.subplots(1, 2)
    plt.plot(X, solve_u)
    mid_x = int(nx / 2)
    analytical_max = np.max(u_exact[:mid_x])
    numerical_max = np.max(solve_u[:mid_x])
    print("numerical value at function max = ", np.abs(numerical_max))
    analytical_min = np.max(u_exact[:mid_x])
    numerical_min = np.max(solve_u[:mid_x])
    plt.plot(X, [solution(x, t_final) for x in X], "--")
    plt.plot(X, np.ones(nx) * numerical_max)
    plt.plot(X, np.ones(nx) * analytical_max)
    #print(solve_u[mid_x:].index(numerical_max / 2))
    plt.xlabel("X")
    plt.title("Diffusion and dispersion on last wave length")
    actual_phase = -1 + dx * np.where(u_exact[:mid_x] == analytical_min)[0][0]
    numerical_phase = -1 + dx * np.where(solve_u[:mid_x] == numerical_min)[0][0]
    plt.axvline(x = numerical_phase, color = "c")
    plt.axvline(x = actual_phase, color = "m")
    print("difference in time hitting function max (phase shift) = ", numerical_phase - actual_phase)
    plt.legend(["U", "u_exact", "numerical max", "exact max", "t at numerical max", "t at exact max"])

   # ax2.plot(X, iter_err)
    plt.show()



    ###########################################################################################
    

elif runMode == "1":
    ############################################################################
    ###### Von Neunman analysis ##################################
    ############################################################################

    fig, axs = plt.subplots(1, 3)

    t_range = np.array([32, 16, 12]) + 1
    h = 2 ** -5

    for i, nt in enumerate(t_range):
        t_final = 1
        dt = t_final / (nt - 1)
        print("Cr = ", a * dt / h)
        T = np.linspace(0, t_final, nt)
        X = np.linspace(-1, -1 + L, int(L / h + 1))
        bc_left = [solution(-1, t) for t in T]
        bc_right = [solution(1, t) for t in T]
        boundary_conditions = (bc_left, bc_right)
        print("dt = ", dt)
        u0 = [solution(x, 0) for x in X][1:-1]
        solve_u, special_err, _ = solve_advection_problem_FTBS(u0, a, h, dt, t_final, boundary_conditions, solution)
        print("done")
        axs[i].plot(X, solve_u)
        axs[i].plot(X, [solution(x, t_final) for x in X], "--")
    axs[1].set_title("diffusion effect")
    axs[0].set_xlabel("Cr < 1, dampening")
    axs[1].set_xlabel("Cr = 1")
    axs[2].set_xlabel("Cr > 1, amplification")
    axs[0].legend(["ftbs", "exact"])
    
    plt.show()


    axs[0, 0].settitle("cr < 1")
    ############################################################################

elif runMode == "2":
    ############################################################################
    ###### convergence test over spacial grid ##################################
    ############################################################################
    
    
    
    # fixed temporal grid
    nt = 1032 + 1     # Number of time steps
    dt = t_final / (nt - 1)
    T = np.linspace(0, t_final, nt)

    # Boundary conditions
    bc_left = [solution(-1, t) for t in T]
    bc_right = [solution(1, t) for t in T]
    boundary_conditions = (bc_left, bc_right)


    #fig, (ax1, ax2, ax3) = plt.subplots(1, 3)
    #fig, (ax1, ax2) = plt.subplots(1, 2)
    err = np.zeros(6)
    grid_whidth = np.array([1 / 2**h for h in range(6, 12)])
    for i, h in enumerate(grid_whidth):

        print("solving for h = ", h)
        print("Cr = ", a * dt / h)

        #print(np.linspace(-1, -1 + L, int(L / h + 1)))
        u0 = [solution(x, 0) for x in np.linspace(-1, -1 + L, int(L / h + 1))][1:-1]
        solve_u, special_err, _ = solve_advection_problem_FTBS(u0, a, h, dt, t_final, boundary_conditions, solution)
        #err[i] = np.max(np.abs(special_err))
        err[i] = np.sqrt(np.sum(np.power(special_err, 2)) / len(special_err))
        
    #ax1.plot(special_err)
    #ax3.plot(solve_u)
    #ax3.plot([solution(x, t_final) for x in np.linspace(-1, -1 + L, int(L / h + 1))], "--")
    plt.loglog(grid_whidth, err,'-o')
    plt.loglog(grid_whidth, grid_whidth * 10, "--")
    plt.xlabel("h")
    plt.ylabel("||E||_2")
    plt.legend(["error" ,"O(h)"])
    plt.title("FTBS convergence test for spacial grid h")
    plt.show()
    



    plt.show()

    ##############################################################################################

elif runMode == "3":
    ##############################################################################################
    ###################### Test for the  dispersion effect     ###################################
    ##############################################################################################



    # running the simulation
    err_time = np.zeros(5)
    grid_whidth = [1 / 2**h for h in range(4, 9)]

    print("--------------- Running convergence test for temporal grid --------------------------")

    for i, k in enumerate(grid_whidth):
        dx =  0.525 *k
 
        u0 = [solution(x, 0) for x in np.linspace(-1, 1, int(L / dx + 1))][1:-1]
        print("solving for k = ", k)
        print("Cr = ", a * k / dx)
        nt = int(t_final / k)
        # Boundary conditions
        T = np.linspace(0, t_final, nt + 1)
        bc_left = [solution(-1, t) for t in T]
        bc_right = [solution(1, t) for t in T]
        boundary_conditions = (bc_left, bc_right)
        solve_u, special_err, iter_err = solve_advection_problem_FTBS(u0, a, dx, k, t_final, boundary_conditions, solution)
        #err_time[i] = np.linalg.norm(iter_err, 2)
        err_time[i] = np.max(iter_err)
    plt.loglog(grid_whidth, err_time, "-o")
    plt.loglog(grid_whidth, np.power(grid_whidth, 1), "--")
    plt.xlabel("k")
    plt.ylabel("||E||_inf")
    plt.legend(["error" ,"O(k)"])
    plt.title("Combined FTBS convergence test (for temporal grid k)")
    plt.show()

    ###########################################################################################

##############################################################################################

else:
##############################################################################################
###################### Test for the dispersion effect ###################################
##############################################################################################
    print("invalid run option and command lien argument")