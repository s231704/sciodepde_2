import numpy as np
import matplotlib.pyplot as plt
from scipy.sparse import diags
from scipy.sparse.linalg import spsolve



# solution
epsilon = 0.1
alphas = [1, 4, 16]
ex_component = lambda t, alpha : np.exp(-epsilon * alpha**2 * t)
cos_component = lambda x, alpha : np.cos(alpha * x)
def solution(x, t):
    ex_array = [ex_component(t, alpha) for alpha in alphas]
    cos_array = [cos_component(x, alpha) for alpha in alphas]
    return np.dot(ex_array, cos_array)

# Function to construct the sparse matrix A for the FTCS scheme
def construct_A(alpha, dx, dt, nx):
    s = alpha * dt / (dx**2)
    diagonals = [s, 1 - 2*s, s]
    offsets = [-1, 0, 1]
    A = diags(diagonals, offsets, shape=(nx, nx)).toarray()
    return A
'''
    # Compute eigenvalues of the matrix A
    eigenvalues = np.linalg.eigvals(A)

    # Plot eigenvalues on the complex plane
    plt.plot(eigenvalues.real, eigenvalues.imag, 'o', markersize=8)
    plt.axhline(0, color='black', linewidth=0.5)
    plt.axvline(0, color='black', linewidth=0.5)
    plt.xlabel('Real Part')
    plt.ylabel('Imaginary Part')
    plt.title('Eigenvalues on Complex Plane')
    plt.grid(True)
    plt.show()
    '''

    

# Function to solve the 1D heat equation using the FTCS scheme with sparse matrix
def solve_heat_equation_FTCS(u0, eps, dx, dt, t_final, boundary_conditions, solution):

    nx = len(u0)
    s = eps * dt / (dx**2)
    A = construct_A(eps, dx, dt, nx)
    timesteps = int(t_final / dt + 1)
    time_error = np.zeros(timesteps + 1)
    X = np.linspace(-1, 1, nx + 2)
    for curr_time in range(timesteps):

        u0 = A@u0
        u0[0] += boundary_conditions[0][curr_time]*s
        u0[-1] += boundary_conditions[1][curr_time]*s
        iter_space_err = np.abs(np.concatenate(([0], np.array(u0) - [solution(x, dt * (curr_time)) for x in X][1:-1], [0]))) 
        time_error[curr_time + 1] = np.sum(np.sqrt(iter_space_err ** 2)/ len(iter_space_err))


    solved_u = np.concatenate((np.array([boundary_conditions[0][-1]]), u0, np.array([boundary_conditions[1][-1]])))
    spacial_error = np.abs(solved_u - [solution(x, t_final) for x in X])
    return solved_u, spacial_error, time_error
